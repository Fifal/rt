<?php
/**
 * Created by PhpStorm.
 * User: Fifal
 * Date: 04.06.2018
 * Time: 20:03
 */

use Nette\Application\UI\Form;

class ProductFormFactory
{
    /**
     * @return Form
     */
    public function create()
    {
        $form = new Form;

        $form->addText('nazev', 'Název projektu')
            ->setHtmlAttribute('class', 'form-control text-control')
            ->setHtmlAttribute('placeholder', 'Projekt Anakonda')
            ->setRequired('Pole název projektu je povinné!');

        $form->addText('datum', 'Datum odevzdání')
            ->setHtmlAttribute('class', 'form-control text-control')
            ->setHtmlAttribute('placeholder', 'dd.mm.YYYY')
            ->addRule(Form::PATTERN, 'Zadejte datum ve formátu dd.mm.YYYY', '^\s*(3[01]|[12][0-9]|0?[1-9])\.(1[012]|0?[1-9])\.((?:19|20)\d{2})\s*$')
            ->setRequired('Pole datum odevzdání je povinné!');

        $form->addSelect('typ_id', 'Typ projektu', array())
            ->setHtmlAttribute('class', 'form-control text-control')
            ->setRequired();

        $form->addCheckbox('web', ' Webový projekt');

        $form->addProtection();


        $renderer = $form->getRenderer();
        $renderer->wrappers['controls']['container'] = NULL;
        $renderer->wrappers['pair']['container'] = 'div class=form-group';
        $renderer->wrappers['pair']['.error'] = 'has-error';
        $renderer->wrappers['control']['container'] = 'div class="col-md-6 offset-md-3"';
        $renderer->wrappers['label']['container'] = 'div class="col-sm-3 control-label offset-sm-3"';
        $renderer->wrappers['control']['description'] = 'span class=help-block';
        $renderer->wrappers['control']['errorcontainer'] = 'span class=help-block';

        return $form;
    }

}