<?php
/**
 * Created by PhpStorm.
 * User: Fifal
 * Date: 06.06.2018
 * Time: 14:35
 */

namespace App\Components;

use App\Model\Model;
use Nette\Application\BadRequestException;

class TableComponent extends \Nette\Application\UI\Control
{
    private $model;

    /** @persistent bool  */
    public $order; // true : ASC, false : DESC

    /**
     * TableComponent constructor.
     *
     * @param Model $context: model
     */
    public function __construct(Model $context)
    {
        parent::__construct();
        $this->model = $context;
        $this->order = true;
    }


    public function render()
    {
        $this->template->setFile(__DIR__ . '/templates/table.latte');

        // On first load set default variables
        if (!isset($this->template->projects)) {
            $this->clearActive();
            $this->template->idActive = 'column-selected';
            $this->template->projects = $this->model->getProjects();
            $this->template->order = $this->order;
        }

        // Component name for table search using AJAX
        $this->template->componentName = $this->getName();
        $this->template->render();
    }

    /**
     *  Function which handles sort, calls model get projects and then order them ASC by given column name
     *
     * @param $column: column name
     */
    public function handleSort($column)
    {
        if ($this->parent->isAjax()) {
            $projects = $this->model->getProjects();
            $this->clearActive();

            // Switch sort order from ASC to DESC or otherwise
            $this->order ? $this->order = false : $this->order = true;

            switch ($column) {
                case 'id':
                    {
                        $this->order ? $projects->order('id ASC') : $projects->order('id DESC');
                        $this->template->idActive = 'column-selected';
                        break;
                    }
                case 'nazev':
                    {
                        $this->order ? $projects->order('nazev ASC') : $projects->order('nazev DESC');
                        $this->template->nazevActive = 'column-selected';
                        break;
                    }
                case 'datum':
                    {
                        $this->order ? $projects->order('datum ASC') : $projects->order('datum DESC');
                        $this->template->datumActive = 'column-selected';
                        break;
                    }
                case 'typ':
                    {
                        $this->order ? $projects->order('typ_id ASC') : $projects->order('typ_id DESC');
                        $this->template->typActive = 'column-selected';
                        break;
                    }
                case 'web':
                    {
                        $this->order ? $projects->order('web ASC') : $projects->order('web DESC');
                        $this->template->webActive = 'column-selected';
                        break;
                    }
                default:
                    {
                        $this->order ? $projects->order('id ASC') : $projects->order('id DESC');
                        $this->template->idActive = 'column-selected';
                    }
            }

            $this->template->projects = $projects;
            $this->template->order = $this->order;

            $this->redrawControl();
        } else {
            $this->parent->redirect('this');
        }
    }

    /**
     * Handles search
     *
     * @param $text
     */
    public function handleSearch($text){
        $projects = $this->model->getProjects()->where('nazev LIKE ?', '%' .$text .'%');

        if($projects->count() == 0){
            $this->flashMessage('Nebyl nalezen žádný výsledek.', 'info');
        }

        $this->template->projects = $projects;
        $this->template->order = $this->order;
        $this->clearActive();

        $this->redrawControl();
    }

    /**
     * Clears columns css class which indicates that table is sorted by given column
     */
    private function clearActive(){
        $this->template->idActive = '';
        $this->template->nazevActive = '';
        $this->template->datumActive = '';
        $this->template->typActive = '';
        $this->template->webActive = '';
    }

    /**
     * Saves state
     *
     * @param array $params Save
     */
    public function saveState(array &$params)
    {
        parent::saveState($params);
    }

    /**
     * Loads state
     *
     * @param array $params
     */
    public function loadState(array $params)
    {
        try {
            parent::loadState($params);
        } catch (BadRequestException $e) {
            $this->flashMessage('Špatný požadavek: ' . $e->getMessage(), 'danger');
        }
    }

}