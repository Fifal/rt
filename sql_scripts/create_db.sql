-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Počítač: localhost:3306
-- Vytvořeno: Stř 06. čen 2018, 16:03
-- Verze serveru: 10.1.29-MariaDB-6
-- Verze PHP: 5.6.33-0+deb8u1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Databáze: `rt_fifal`
--
CREATE DATABASE IF NOT EXISTS `rt_fifal` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `rt_fifal`;

-- --------------------------------------------------------

--
-- Struktura tabulky `PROJEKT`
--

DROP TABLE IF EXISTS `PROJEKT`;
CREATE TABLE `PROJEKT` (
  `id` int(11) NOT NULL,
  `typ_id` int(11) NOT NULL,
  `nazev` varchar(50) NOT NULL,
  `datum` date NOT NULL,
  `web` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabulky `TYP`
--

DROP TABLE IF EXISTS `TYP`;
CREATE TABLE `TYP` (
  `id` int(11) NOT NULL,
  `nazev` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Klíče pro exportované tabulky
--

--
-- Klíče pro tabulku `PROJEKT`
--
ALTER TABLE `PROJEKT`
  ADD PRIMARY KEY (`id`),
  ADD KEY `typ_id` (`typ_id`);

--
-- Klíče pro tabulku `TYP`
--
ALTER TABLE `TYP`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pro tabulky
--

--
-- AUTO_INCREMENT pro tabulku `PROJEKT`
--
ALTER TABLE `PROJEKT`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;
--
-- AUTO_INCREMENT pro tabulku `TYP`
--
ALTER TABLE `TYP`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- Omezení pro exportované tabulky
--

--
-- Omezení pro tabulku `PROJEKT`
--
ALTER TABLE `PROJEKT`
  ADD CONSTRAINT `PROJEKT_ibfk_1` FOREIGN KEY (`typ_id`) REFERENCES `TYP` (`id`);
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Databáze: `rt_fifal`
--

INSERT INTO `TYP` (`id`, `nazev`) VALUES
(1, 'časově omezený projekt'),
(2, 'Continuous Integration');


INSERT INTO `PROJEKT` (`id`, `typ_id`, `nazev`, `datum`, `web`) VALUES
(29, 2, 'A Projekt', '2012-01-01', 1),
(30, 1, 'B Projekt', '2017-05-05', 1),
(31, 1, 'C Projekt', '2010-08-15', 0),
(32, 2, 'D Projekt', '2018-08-15', 0),
(33, 1, 'Z Projekt', '2014-09-01', 1),
(34, 1, 'X Projekt', '2018-06-06', 0),
(35, 2, 'U Projekt', '2017-02-01', 0);